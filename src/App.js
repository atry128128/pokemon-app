import React from "react";
import './styles/app-styles.css';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import Home from "./components/Home";
import Game from "./components/Game";

function App() {
    return (
        <div className="App">
            <Router>
                    <Switch>
                        <Route path="/game">
                            <Game/>
                        </Route>
                        <Route path="/">
                            <Home/>
                        </Route>
                    </Switch>
            </Router>
        </div>
    );
}






export default App;

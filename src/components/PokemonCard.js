import React from "react";
import PokebolIcon from '../icons/pokeball.png';
import Spinner from '../icons/spinner.png';

const PokemonCard = props => {
    const namePokemonParsed = props.namePokemon.toUpperCase().replace('-', ' ');

    return (
        <div className={'pokemon-card'}>
            <div>
                <div className={'name-pokemon-container'}>
                    <img alt='' src={PokebolIcon}/>{namePokemonParsed}
                </div>
            </div>
            {props.artworkPokemon ?
                <img alt='' src={props.artworkPokemon}/>
                :
                <img alt='' className={'spinner'} src={Spinner}/>
            }
        </div>
    )
}

export default PokemonCard;
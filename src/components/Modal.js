import React from "react";
import {Modal} from "react-bootstrap";
import '../styles/modal-styles.css';
import PokebolIcon from "../icons/pokeball.png";

const ModalFinal = props => {
    const {show, onHide, finalObject} = props;
    const pokemonsAmount = finalObject?.pokemons.length;

    return (
        <Modal show={show} onHide={onHide}>
            <Modal.Header closeButton>
                <Modal.Title>
                    {pokemonsAmount !== 0 ?
                        `BRAWO ${finalObject?.nickname}!`
                        :
                        `UNFORTUNATELY :<`}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div>
                    <div className={'amount-pokemons-info'}>
                        You cought {pokemonsAmount} pokemon{pokemonsAmount !== 1 ? 's' : ""}!
                    </div>
                    {finalObject?.pokemons.map((pokemon, key) =>
                        <div key={key}>
                            <img alt='' className={'pokemon-icon'} src={PokebolIcon}/>
                            {pokemon}
                        </div>)
                    }
                </div>
            </Modal.Body>
        </Modal>
    )
}

export default ModalFinal;
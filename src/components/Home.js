import React from "react";
import {
    Col,
    Button,
    InputGroup,
    Form,
    Container
} from 'react-bootstrap';
import {useHistory} from "react-router-dom";
import {connect} from "react-redux";
import '../styles/home-styles.css';
import {setName as setNameRedux} from "../redux/actions";

const Home = props => {
    const [name, setName] = React.useState("");
    const [validated, setValidated] = React.useState(false);
    const history = useHistory();

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        } else {
            props.setNameRedux(name);
            history.push('/game');
        }
        setValidated(true);
    };

    return (
        <Container className={'home-container'}>
            <a href="https://fontmeme.com/pokemon-font/">
                <img
                    className={'logo'}
                    src="https://fontmeme.com/permalink/210116/2c0479c74ecb90f9d1bf3f8ea91bc9d2.png" alt="pokemon-font"
                    border="0"/></a>
            <Form className={'form-container'} noValidate validated={validated} onSubmit={handleSubmit}>
                <Form.Group as={Col}>
                    <Form.Label className={'label-form'}>enter your nickname</Form.Label>
                    <div className={'input-container'}>
                    <InputGroup className={'input'}>
                        <Form.Control
                            type="text"
                            onChange={e => setName(e.target.value)}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            Nickname is required
                        </Form.Control.Feedback>
                    </InputGroup>
                    </div>
                </Form.Group>
                <Button className={'button'} type="submit">
                    Continue
                </Button>
            </Form>
        </Container>
    )
}

export default connect(null, {setNameRedux})(Home);


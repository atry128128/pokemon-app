import React from "react";
import PokemonCard from "./PokemonCard";

const Pokemon = props => {
    const [pokemonApi, setPokemonApi] = React.useState(null);

    React.useEffect(() => {
        const URL_API = props.pokemon.url.toString();
        fetch(URL_API)
            .then(response => (response.json()))
            .then(data => setPokemonApi(data));
    }, []);

    const pokemon = {
        name: props.pokemon.name,
        artwork: pokemonApi?.sprites?.other['official-artwork'].front_default,
        stats: pokemonApi?.stats.map(
            element => {
                return {
                    name: element.stat.name,
                    value: element.base_stat
                };
            }
        )
    };

    return <PokemonCard namePokemon={pokemon.name} artworkPokemon={pokemon.artwork}/>;
}

export default Pokemon;
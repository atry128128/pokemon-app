import React from "react";
import {getName, getPokemons} from '../redux/selectors';
import {connect} from "react-redux";
import PokemonList from "./PokemonList";
import Modal from "./Modal";
import {Button} from "react-bootstrap";
import {setPokemons as setPokemonsRedux} from "../redux/actions";

const Game = props => {
    const [showModal, setShowModal] = React.useState(false);
    const [finalObject, setFinalObject] = React.useState(null);

    const handleCloseModal = () => setShowModal(false);
    const handleShowModal = () => setShowModal(true);

    function catchPokemons() {
        const caughtPokemons = [];
        props.pokemons?.map(pokemon => {
            const caughtOrNotCaught = Math.floor(Math.random() * 2);
            if (caughtOrNotCaught === 1) caughtPokemons.push(pokemon.name);
        })
        createFinalObject(caughtPokemons);
    }

    function createFinalObject(caughtPokemons) {
        const finalObject = {
            nickname: props.name,
            pokemons: caughtPokemons
        }
        setFinalObject(finalObject);
        handleShowModal();
        console.log(finalObject);
    }

    return (
        <div className={'game-container'}>
            <div>
                Hello, {props.name}!
            </div>
            <PokemonList/>
            <Button className={'button'} onClick={() => catchPokemons()}>Catch!</Button>
            <Modal
                show={showModal}
                onHide={handleCloseModal}
                finalObject={finalObject}/>
        </div>
    )
}

const mapPropsToState = state => ({
    name: getName(state),
    pokemons: getPokemons(state)
});

export default connect(mapPropsToState, {setPokemonsRedux})(Game);
import React from "react";
import {getName, getPokemons} from '../redux/selectors';
import {connect} from "react-redux";
import Pokemon from './Pokemon';
import {setPokemons as setPokemonsRedux} from "../redux/actions";
import '../styles/game-styles.css'

const PokemonList = props => {

    React.useEffect(() => {
        const usernameLength = props.name.length;
        const offset = usernameLength * 10;
        const URL_API = `https://pokeapi.co/api/v2/pokemon?limit=5&offset=${offset}`;

        fetch(URL_API)
            .then(response => (response.json()))
            .then(data => props.setPokemonsRedux(data.results));
    }, []);

    return (
        <div className={'pokemon-list-container'}>
            {props.pokemons?.map((pokemon, key) => <Pokemon key={key} pokemon={pokemon}/>)}
        </div>
    );
}

const mapPropsToState = state => ({
    name: getName(state),
    pokemons: getPokemons(state)
});

export default connect(mapPropsToState, {setPokemonsRedux})(PokemonList);
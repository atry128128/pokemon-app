import { SET_NAME, SET_POKEMONS } from "./actionTypes";


export const setName = userName => ({
    type: SET_NAME,
    payload: {
        userName
    }
});

export const setPokemons = pokemons => ({
    type: SET_POKEMONS,
    payload: {
        pokemons
    }
});




export const getName = store => store.mainReducer?.name;
export const getPokemons = store => store.mainReducer?.pokemons;
import {SET_NAME, SET_POKEMONS} from "../actionTypes";

const initialState = {
    name: "",
    pokemons: []
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_NAME: {
            return {...state, name: action.payload.userName};
        }
        case SET_POKEMONS: {
            return {...state, pokemons: action.payload.pokemons}
        }
        default:
            return state;
    }
}
